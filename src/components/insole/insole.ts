import {Component, ElementRef} from '@angular/core';
import { ViewChild, Input } from '@angular/core';

import * as d3 from "d3";
import * as d3Contour from "d3-contour";
import {Converter} from "../../services/converter";
import {InsolesProvider} from "../../providers/insoles";

declare let kriging: any;

/**
 * Generated class for the InsoleComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'insole',
    templateUrl: 'insole.html',
})
export class InsoleComponent {

    @Input('isLeft') isLeft: boolean;
    @Input('hideLegend') hideLegend: boolean = false;

    @Input('slideId') slideId: number;
    @Input('currentIndex') currentIndex: number;

    @ViewChild('insole') insole: ElementRef;
    @ViewChild('legend_svg') legend: ElementRef;
    @ViewChild('canvas') canvas: ElementRef;
    constructor(
        public insolesData: InsolesProvider
    ){

    }

    ngAfterContentInit() {

    }

    ngAfterViewInit() {

        // viewChildren is set
        //console.log('viewChildren is set Insole');
        //console.log('this.isLeft',this.isLeft);

        let insole = this.insole.nativeElement;
        let maxADC = 607;

        let left_x = [ 3,7,3,3,5,7,5,7,3,5,7,5,7,5,7,7,3,5,7,5,7,3,5,7 ];
        let left_y = [ 3,3,5,7,7,7,11,11,13,13,13,15,15,17,17,19,21,21,21,23,23,24,24,24 ];

        // let left_x = [ 3,7,3,3  ,7,5,7,3,5,7,5,7,5,7,7,3,  7,5,7,3,5,7 ];
        // let left_y = [ 3,3,5,7  ,7,11,11,13,13,13,15,15,17,17,19,21  ,21,23,23,24,24,24 ];

        let right_x = [ 3,7,7,3,5,7,3,5,3,5,7,3,5,3,5,3,3,5,7,3,5,3,5,7 ];
        let right_y = [ 3,3,5,7,7,7,11,11,13,13,13,15,15,17,17,19,21,21,21,23,23,24,24,24 ];


        let n = 8, m = 24;

        let model = "exponential";
        let sigma2 = 0, alpha = 100;

        function predict(x, y , variogram ) {
            return kriging.predict(x, y, variogram);
        }

        let thresholds = d3.range( 0 ,  maxADC  , 50);

        let contours = d3Contour.contours()
            .size([n, m]);

        let canvas = this.canvas.nativeElement;
        let canvas_height = insole.clientHeight - 180;
        let canvas_width = canvas_height * n / m;
        let h_scale = canvas_height/m;

        d3.select(canvas)
            .attr("width", canvas_width )
            .attr("height", canvas_height );

        let context = canvas.getContext("2d");
        context.scale(h_scale,h_scale);

        let interpolateTerrain = d3.interpolateInferno;
        let color = d3.scaleSequential( interpolateTerrain ).domain(d3.extent(thresholds));

        let path = d3.geoPath(null, context);

        if(!this.isLeft){
            canvas.classList.add('right');
        }

        d3.timer(()=>{

            //console.log('currentIndex=',this.currentIndex);
            //console.log('is currentIndex',this.currentIndex == this.slideId);
            if( this.currentIndex == this.slideId ){

                let variogram = null;
                if(this.isLeft){

                    let left = this.insolesData.getDataLeft();
                    let sensors = left.sensors;
                    let t = [ 0,sensors[0],0,sensors[1],sensors[2],sensors[3],0,0,sensors[4],0,0,0,0,0,0,0,sensors[5],sensors[6],0,sensors[7],0,0,0,0 ];
                    // let t = [
                    //     0,sensors[0],0, sensors[1],//sensors[2],
                    //     sensors[3],0,0,sensors[4],0,
                    //     0,0,0,0,0,
                    //     0,sensors[5],//sensors[6],
                    //     0,sensors[7],0,
                    //     0,0,0 ];
                    variogram = kriging.train(t, left_x, left_y, model, sigma2, alpha);
                } else {

                    let right = this.insolesData.getDataRight();
                    let sensors = right.sensors;
                    let t = [ sensors[0],0, 0, sensors[1],sensors[2],sensors[3], 0,0, 0,0,sensors[4], 0,0, 0,0, 0, 0,sensors[5],sensors[6], 0,sensors[7], 0,0,0 ];
                    variogram = kriging.train(t, right_x, right_y, model, sigma2, alpha);
                }
                let values = new Array(n * m);
                for (let j = 0.5, k = 0; j < m; ++j) {
                    for (let i = 0.5; i < n; ++i, ++k) {
                        values[k] = predict(i, j , variogram );
                        values[k] = values[k] > 0 ? values[k] : 0;
                    }
                }

                contours.thresholds(thresholds)
                (values).forEach(fill);
            }
        });

        function fill(geometry) {
            context.beginPath();
            path(geometry);
            context.fillStyle = color(geometry.value);
            context.fill();
        }

        if(!this.hideLegend){

        let legend = this.legend.nativeElement;
        let legendSvg = d3.select(legend)
            .append("svg")

        // add the legend now
        let legendFullHeight = Math.floor(canvas_height/5*2);
        let legendFullWidth = 50;

        d3.select(legend).attr('style','margin-top: -' + Math.floor(legendFullHeight/2) + 'px');

        let legendMargin = { top: 20, bottom: 20, left: 5, right: 20 };

        // use same margins as main plot
        // let legendWidth = legendFullWidth - legendMargin.left - legendMargin.right;
        let legendHeight = legendFullHeight - legendMargin.top - legendMargin.bottom;

        legendSvg.attr('width', legendFullWidth)
            .attr('height', legendFullHeight)

        // create a scale and axis for the legend
        let legendScale = d3.scaleLinear()
            .domain([ 0 , toKilogram( maxADC ) ])
            .range([legendHeight, 0]);

        let sequentialScale = d3.scaleSequential( interpolateTerrain ).domain(d3.extent(thresholds));

        let blockHeight = Math.ceil(legendHeight/( maxADC /50));

        legendSvg.selectAll('rect')
            .data(d3.range( 0 ,  maxADC  , 50))
            .enter()
            .append('rect')
            .attr('y', function(d) {
                return d3.scaleLinear()
                    .domain([ 0 ,  maxADC  ])
                    .range([legendHeight, 0])(d);
            })
            .attr('width', 11)
            .attr('height', blockHeight)
            .style('fill', function(d) {
                return sequentialScale(d);
            });


        legendSvg.append("g")
            .attr("class", "axisRed")
            .attr('transform', 'translate(11,11)')
            .call(d3.axisRight(legendScale));

        }
    }

}

function toKilogram(x) {
    return (new Converter).toKilogram(x);
}
