import {Component, ElementRef, ViewChild} from '@angular/core';
import { Input } from '@angular/core';
import { DecimalPipe } from "@angular/common";
import * as d3 from "d3";
import {WeightProvider} from "../../providers/weight";

/**
 * Generated class for the WeightShiftComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'weight-shift',
  templateUrl: 'weight-shift.html'
})
export class WeightShiftComponent {

  @Input('slideId') slideId: number;
  @Input('currentIndex') currentIndex: number;
  @ViewChild('container') container: ElementRef;

  gauge: any[];
  weight_LR: any[];

  gauge_view: any[] = [350, 100];
  bar_view: any[] = [350, 200];

  valueFormatting = function(a){
    let p = new DecimalPipe('en');
    return p.transform(a,'1.2-2');
  }

  constructor(
    private weight: WeightProvider
  ) {

    this.gauge = [
      {
        "name": "Weight",
        "value": 0
      }
    ]

    this.weight_LR = [
      {
        "name": "Left",
        "value": 0
      },
      {
        "name": "Right",
        "value": 0
      }
    ];

    Object.assign( this , { "gauge": this.gauge , "weight_LR": this.weight_LR  });

  }

  ngAfterViewInit() {

    let container = this.container.nativeElement;
    let container_width = container.clientWidth;

    this.gauge_view  = [
      container_width,
      container_width/1.5
    ];
    this.bar_view = [
      container_width/2,
      container_width * 3/4
    ];

    d3.timer(()=> {

      if (this.currentIndex == this.slideId) {
          this.gauge = [...this.setGaugeValue(this.weight.data['sum_all'])];
          let val1 = this.weight.data['sum_l'];
          let val2 = this.weight.data['sum_r'];
          this.weight_LR = [...this.setWeight_LRValue(val1,val2)];
      }
    },100);
  }

  setGaugeValue(val) {
      this.gauge[0]["value"] = val;
      return this.gauge;
  }
  setWeight_LRValue(val1,val2) {
      this.weight_LR[0]["value"] = val1;
      this.weight_LR[1]["value"] = val2;
      return this.weight_LR;
  }

}
