import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {BLE} from "@ionic-native/ble";
import {HomePage} from "../pages/home/home";
import {Converter} from "../services/converter";
import {IonicStorageModule} from "@ionic/storage";
import { InsolesProvider } from '../providers/insoles';
import { WeightProvider } from "../providers/weight";

@NgModule({
    bootstrap: [IonicApp],
    entryComponents: [MyApp,HomePage],
    declarations: [MyApp,HomePage],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot()
    ],
    providers: [
        StatusBar,
        SplashScreen,
        BLE,
        Converter,
        WeightProvider,
        InsolesProvider,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
